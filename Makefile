## Markdown extension (e.g. md, markdown, mdown).
MEXT = md

## All markdown files in the working directory
SRC = $(wildcard *.$(MEXT))

## Header file
DEPS = metadata.yaml

## Location of Pandoc support files.
PREFIX = /Users/xuliang/Documents/Zotero

## CSL stylesheet (located in the csl folder of the PREFIX directory).
CSL = 00draft_style

## Location of your working bibliography file
BIB = my_bbt.bib

## Flags
# FLAGS = -s -S --filter /Users/xuliang/Library/Haskell/bin/pandoc-crossref --filter pandoc-citeproc --bibliography=$(PREFIX)/$(BIB)  --csl=$(PREFIX)/styles/$(CSL).csl --number-sections
FLAGS = -s -S --filter /Users/xuliang/Library/Haskell/bin/pandoc-csv2table --filter /Users/xuliang/Library/Haskell/bin/pandoc-crossref --filter pandoc-citeproc --bibliography=$(PREFIX)/$(BIB)  --csl=$(PREFIX)/styles/$(CSL).csl --number-sections


PDFS=$(SRC:.md=.pdf)
HTML=$(SRC:.md=.html)
DOCX=$(SRC:.md=.docx)

all:	$(PDFS) $(HTML) $(DOCX)

pdf:	clean $(PDFS)
html:	clean $(HTML)
docx:	clean $(DOCX)

%.html:	%.md
	pandoc --mathjax $(FLAGS) -o $@ $< $(DEPS)
%.pdf:	%.md
	pandoc --template=$(PREFIX)/templates/paper.latex $(FLAGS) -o $@ $< $(DEPS)
%.docx:	%.md
	pandoc --reference-docx=$(PREFIX)/templates/apa.docx $(FLAGS) -o $@ $< $(DEPS)

clean:
	rm -f *.html *.pdf *.docx
