
# Introduction

* What happened

* Current techniques

* Proposed methods


# Materials and Methods

## Data

1. Amazon seasonality map derived from the climatology of 2000-2009 [@xu_satellite_2015]

1. QSCAT and OceanSAT
    - QSCAT from 2000 to 2009
    - OSAT from 2010 to 2014-3

## Method


# Results


# References {-}
